$SG5199 DB        'Less than', 00H
        ORG $+2
$SG5202 DB        'Equals', 00H
        ORG $+5
$SG5203 DB        'Greater than', 00H
unsigned __int64 `__local_stdio_printf_options'::`2'::_OptionsStorage DQ 01H DUP (?) ; `__local_stdio_printf_options'::`2'::_OptionsStorage

a$ = 48
void func(unsigned __int64) PROC                           ; func
$LN7:
        mov     QWORD PTR [rsp+8], rcx
        sub     rsp, 40                             ; 00000028H
        cmp     QWORD PTR a$[rsp], 5
        jae     SHORT $LN2@func
        lea     rcx, OFFSET FLAT:$SG5199
        call    printf
        jmp     SHORT $LN3@func
$LN2@func:
        cmp     QWORD PTR a$[rsp], 5
        jne     SHORT $LN4@func
        lea     rcx, OFFSET FLAT:$SG5202
        call    printf
        jmp     SHORT $LN5@func
$LN4@func:
        lea     rcx, OFFSET FLAT:$SG5203
        call    printf
$LN5@func:
$LN3@func:
        add     rsp, 40                             ; 00000028H
        ret     0
void func(unsigned __int64) ENDP                           ; func